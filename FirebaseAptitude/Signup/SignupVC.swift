//
//  SignupVC.swift
//  FirebaseAptitude
//
//  Created by Ashikur Rahman on 12/11/21.
//

import UIKit
import FirebaseAuth

class SignupVC: UIViewController {
    
    
    private var iconClick = true
    
    @IBOutlet weak var nameTxtField:UITextField!{
        didSet{
            self.nameTxtField.delegate = self
        }
    }
    @IBOutlet weak var emailTxtField:UITextField!{
        didSet{
            self.emailTxtField.delegate = self
        }
    }
    @IBOutlet weak var passwordTxtField:UITextField!{
        didSet{
            self.passwordTxtField.delegate = self
        }
    }
    
    @IBOutlet weak var btnSignup:UIButton!{
        didSet{
            self.btnSignup.addTarget(self, action: #selector(btnSignupAction), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var lblAlreadyRegistered:UILabel!{
        didSet{
            self.setAttributedText()
        }
    }
    
    @IBOutlet weak var btnShowORHidePass:UIButton!{
        didSet{
            self.btnShowORHidePass.addTarget(self, action: #selector(btnShowORHidePassAction), for: .touchUpInside)
        }
    }
    
    
    
    @IBOutlet weak var alreadyLoginView:UIView!{
        didSet{
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(tapGestureRecognizer:)))
            self.alreadyLoginView.addGestureRecognizer(tap)
            self.alreadyLoginView.isUserInteractionEnabled = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.passwordTxtField.isSecureTextEntry = true
        self.hideKeyboardWhenTappedAround()
    }
    
}

extension SignupVC{
    
    @objc func handleTap(tapGestureRecognizer: UITapGestureRecognizer){
        guard let vc = self.getViewContoller(storyboardName: Storyboard.Login, viewControllerName: "LoginVC") as? LoginVC else { return  }
        self.pushTheView(vc: vc)
    }
    
    
    @objc private func btnSignupAction(){
        self.firebaseSignup()
    }
    private func firebaseSignup(){
        
        if !Reach().isInternetAvailable(){
            self.noInternetAlert()
            return
        }
        
        
        guard let email = self.emailTxtField.text,let password = self.passwordTxtField.text else {
            self.showAlertWithMessage(title: "Alert", message: "Invalid email or Wrong password", actionTitle: "OK")
            return
            
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let error = error as NSError?{
                switch AuthErrorCode(rawValue: error.code) {
                case .operationNotAllowed:
                    break
                case .emailAlreadyInUse:
                    break
                case .invalidEmail:
                    break
                case .weakPassword:
                    break
                default:
                    print("Error: \(error.localizedDescription)")
                }
            }else {
                print("User signs up successfully")
                let newUserInfo = Auth.auth().currentUser
                let email = newUserInfo?.email
                
                
                
                guard let vc = self.getViewContoller(storyboardName: Storyboard.Login, viewControllerName: "LoginVC") as? LoginVC else { return  }
                self.pushTheView(vc: vc)
              }
        }
        
        
    }
    @objc private func btnShowORHidePassAction(){
        if(iconClick == true) {
            self.passwordTxtField.isSecureTextEntry = false
        } else {
            self.passwordTxtField.isSecureTextEntry = true
        }
        
        self.iconClick = !self.iconClick
    }
    
}


extension SignupVC{
    private func setAttributedText(){
        
        
        let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        

        let partOne = NSMutableAttributedString(string: "Already have an account? - ", attributes: yourAttributes)

        let combination = NSMutableAttributedString()

        combination.append(partOne)
        
        
        
        let underlineAttribute = [NSAttributedString.Key.foregroundColor: UIColor.red,NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue] as [NSAttributedString.Key : Any]
        let underlineAttributedString = NSAttributedString(string: "Login", attributes: underlineAttribute)
        combination.append(underlineAttributedString)
        self.lblAlreadyRegistered.attributedText = combination
    }
}

extension SignupVC:UITextFieldDelegate{
    
}
