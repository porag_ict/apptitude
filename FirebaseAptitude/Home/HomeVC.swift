//
//  HomeVC.swift
//  FirebaseAptitude
//
//  Created by Ashikur Rahman on 12/11/21.
//

import UIKit
import GoogleMaps
import AVFoundation
import AVKit
import FirebaseAuth

class HomeVC: UIViewController {
    
    var timer = Timer()
    private var audioDataSource = [AudioEntity]()
    
    
    private var currentSelectedIndex = -1
    
    @IBOutlet weak var cnstBtmPlayerView:NSLayoutConstraint!
    @IBOutlet weak var cnstBtmRecordView:NSLayoutConstraint!
    
    @IBOutlet weak var lblCurrentRecordName:UILabel!
    @IBOutlet weak var lblCurrentRecordTime:UILabel!
    
    @IBOutlet weak var btnStopRecord:UIButton!{
        didSet{
            self.btnStopRecord.addTarget(self, action: #selector(btnStopRecordAction), for: .touchUpInside)
        }
    }
    
    
    let locationManager = CLLocationManager()
    
    
    var startButtonState = 0
    
    var audioURLFinal:URL?
    
    private var audioRecordManager = AudioRecordManager.shared
    
    @IBOutlet weak var btnFavorite:UIButton!{
        didSet{
            self.btnFavorite.addTarget(self, action: #selector(btnFavoriteAction), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnNotification:UIButton!{
        didSet{
            self.btnNotification.addTarget(self, action: #selector(btnNotificationAction), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnLogoout:UIButton!{
        didSet{
            self.btnLogoout.addTarget(self, action: #selector(btnLogooutAction), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var btnRecord:UIButton!{
        didSet{
            self.btnRecord.addTarget(self, action: #selector(btnRecordAction), for: .touchUpInside)
        }
    }
    
    
    @IBOutlet weak var btnPlayPause:UIButton!{
        didSet{
            self.btnPlayPause.addTarget(self, action: #selector(btnPlayPauseAction), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var btnFastForward:UIButton!{
        didSet{
            self.btnFastForward.addTarget(self, action: #selector(btnFastForwardAction), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnFastBackward:UIButton!{
        didSet{
            self.btnFastBackward.addTarget(self, action: #selector(btnFastBackwardAction), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var lblRecordName:UILabel!{
        didSet{
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
            self.lblRecordName.addGestureRecognizer(tapGesture)
            self.lblRecordName.isUserInteractionEnabled = true
            
        }
    }
    
    @IBOutlet weak var volumeSlider:UISlider!{
        didSet{
            self.volumeSlider.addTarget(self, action: #selector(self.volumeSliderAction(_:_:)), for: .valueChanged)
        }
    }
    
    
    @IBOutlet weak var lblCurrentTime:UILabel!
    @IBOutlet weak var lblRemainingTime:UILabel!
    
    
    
    @IBOutlet weak var googleMapView:GMSMapView!{
        didSet{
            self.googleMapView.delegate = self
        }
    }
    
    
    @IBOutlet weak var addressLabel:UILabel!
    
    var recordTime = 0
    var videoDuration:Float = 0.1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewDidLoad()
    }
    
    private func setupViewDidLoad(){
        
        self.cnstBtmPlayerView.constant = -UIScreen.main.bounds.height/2
        self.cnstBtmRecordView.constant = -UIScreen.main.bounds.height/2
        self.view.layoutIfNeeded()
        
        self.locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.requestLocation()
            self.googleMapView.isMyLocationEnabled = true
            self.googleMapView.settings.myLocationButton = true
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        
        self.audioDataSource = CoreDataManager.shared.getMediaListFromCoreData()
        
        
        if self.audioDataSource.count > 0{
            self.btnFastForward.isEnabled = true
            self.btnFastBackward.isEnabled = false
        }else{
            self.btnFastForward.isEnabled = false
            self.btnFastBackward.isEnabled = false
        }
    }
    
    @objc private func handleTap(sender: UITapGestureRecognizer) {
        guard let name = self.lblRecordName.text else { return }
        self.renameAction(audioTitle: name)
    }
    
    @objc private func volumeSliderAction(_ sender: UISlider,_ event:UIEvent){
        
        
        
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
//                self.volumeSlider.transform = CGAffineTransform(scaleX: 1, y: 1.5)
                PlayerManager.sharedInstance.playerPause()
                break
            case .moved:
                break
            case .ended:
                print("")
                PlayerManager.sharedInstance.seekAudio(to: CGFloat(self.volumeSlider.value * self.videoDuration))
                PlayerManager.sharedInstance.playerPlay()
            default:
                break
            }
        }
    }
    
}

extension HomeVC{
    @objc private func btnPlayPauseAction(){
        if PlayerManager.sharedInstance.playerIsPlaying(){
            
            self.btnPlayPause.setTitle("Play", for: .normal)
            PlayerManager.sharedInstance.playerPause()
        }else{
            PlayerManager.sharedInstance.playerPlay()
            self.btnPlayPause.setTitle("Pause", for: .normal)
        }
    }
    @objc private func btnFastForwardAction(){
        
        self.currentSelectedIndex += 1
        if self.currentSelectedIndex >= 0 && self.currentSelectedIndex < self.audioDataSource.count{
            
            
            
            self.btnFastForward.isEnabled = true
            self.btnFastBackward.isEnabled = true
//            self.playAudio(url: self.songList[self.currentSelectedIndex])
            
            let audio = self.audioDataSource[self.currentSelectedIndex]
            if let lastPath = audio.audioURL,let url = DocManager.shared.getAudioURL(mediaName: lastPath){
                self.setVideoDuration(url: url)
                self.lblRecordName.text = self.audioDataSource[self.currentSelectedIndex].audioName
                
                self.playAudio(url: url)
            }
            
        }else{
            self.currentSelectedIndex = -1
            self.btnFastForward.isEnabled = false
            self.btnFastBackward.isEnabled = true
        }
        
    }
    @objc private func btnFastBackwardAction(){
        
        self.currentSelectedIndex -= 1
        if self.currentSelectedIndex >= 0 && self.currentSelectedIndex < self.audioDataSource.count{
            self.btnFastForward.isEnabled = true
            self.btnFastBackward.isEnabled = true
//            self.playAudio(url: self.songList[self.currentSelectedIndex])
            let audio = self.audioDataSource[self.currentSelectedIndex]
            if let lastPath = audio.audioURL,let url = DocManager.shared.getAudioURL(mediaName: lastPath){
                self.setVideoDuration(url: url)
                self.lblRecordName.text = self.audioDataSource[self.currentSelectedIndex].audioName
                self.playAudio(url: url)
            }
        }else{
            self.currentSelectedIndex = -1
            self.btnFastForward.isEnabled = true
            self.btnFastBackward.isEnabled = false
        }
    }
    
    
    
    private func playAudio(url:URL){
        PlayerManager.sharedInstance.playerPause()
        PlayerManager.sharedInstance.audioPlayer = nil
        PlayerManager.sharedInstance.delegate = self
        PlayerManager.sharedInstance.playAudio(withUrl: url as NSURL)
    }
    
    
    @objc private func btnStopRecordAction(){
        self.stopRecording()
//        self.btnRecord.setTitle("Start Record", for: .normal)
    }
    @objc private func btnFavoriteAction(){
        
    }
    @objc private func btnNotificationAction(){
        
    }
    @objc private func btnLogooutAction(){
        if !Reach().isInternetAvailable(){
            self.noInternetAlert()
            return
        }
        
        do {
            try Auth.auth().signOut()
            appDelegate.setRootVC()
        } catch {
            print("Sign out error")
        }
    }
    @objc private func btnRecordAction(){
        self.btnStartRecordAction()
    }
    
}


extension HomeVC{
    @objc private func btnStartRecordAction(){
        if self.audioRecordManager.isRecording == false {
            
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateRecordTime), userInfo: nil, repeats: true)
            
            self.audioRecordManager.startRecord()
            self.lblCurrentRecordName.text = audioRecordManager.fileName
            
            self.cnstBtmRecordView.constant = 0
            
            self.animateView()
            
            
//            self.btnRecord.setTitle("Stop Record", for: .normal)
        }
    }
    
    @objc private func updateRecordTime(){
        
        recordTime += 1
        self.lblCurrentRecordTime.text = TimeFormatter.timeInSeconds(time: recordTime)
    }
    
    private func animateView(){
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    private func setVideoDuration(url:URL){
        let asset = AVAsset(url: url)
        self.videoDuration = Float(asset.duration.seconds)
    }
    
    private func stopRecording(){
        self.timer.invalidate()
        self.recordTime = 0
        if audioRecordManager.isRecording {
            
            self.audioRecordManager.stopRecording()
            
            DispatchQueue.main.async {
                
                if let url = self.audioRecordManager.getAudioFileUrl() {
                    
                    self.audioURLFinal = url
                    if let name = self.audioRecordManager.fileName{
                        let item = CoreDataManager.shared.addMediaToCoreData(creationDate: Date(), fileName: name, fileURL: name + ".m4a")
                        if let audio = item{
                            self.audioDataSource.insert(audio, at: 0)
                            self.lblRecordName.text = name
                            self.lblCurrentRecordName.text = name
                            
                            self.setVideoDuration(url: url)
                            self.playAudio(url: url)
                            self.currentSelectedIndex = 0
                            self.cnstBtmPlayerView.constant = 0
                            self.cnstBtmRecordView.constant = -UIScreen.main.bounds.height/2
                            
                            self.animateView()
                        }
                        
                    }
                }
            }
        }
    }
}




extension HomeVC:CLLocationManagerDelegate{
    func locationManager(
        _ manager: CLLocationManager,
        didChangeAuthorization status: CLAuthorizationStatus
    ) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.requestLocation()
        self.googleMapView.isMyLocationEnabled = true
        self.googleMapView.settings.myLocationButton = true
    }
    func locationManager(
        _ manager: CLLocationManager,
        didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        self.googleMapView.camera = GMSCameraPosition(
            target: location.coordinate,
            zoom: 15,
            bearing: 0,
            viewingAngle: 0)
    }
    
    func locationManager(
        _ manager: CLLocationManager,
        didFailWithError error: Error
    ) {
        print(error)
    }
}


extension HomeVC{
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
      let geocoder = GMSGeocoder()
      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        self.addressLabel.unlock()
        
        guard let address = response?.firstResult(),let lines = address.lines else {return}
        
        self.addressLabel.text = lines.joined(separator: "\n")
        UIView.animate(withDuration: 0.25) {
          self.view.layoutIfNeeded()
        }
      }
    }

}


extension HomeVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
      reverseGeocode(coordinate: position.target)
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.addressLabel.lock()
    }
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
         
         //Creating Marker
         let marker = GMSMarker(position: coordinate)
        
         let decoder = CLGeocoder()

         //This method is used to get location details from coordinates
         decoder.reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { placemarks, err in
            if let placeMark = placemarks?.first {

                let placeName = placeMark.name ?? placeMark.subThoroughfare ?? placeMark.thoroughfare!   ///Title of Marker
                //Formatting for Marker Snippet/Subtitle
                var address : String! = ""
                if let subLocality = placeMark.subLocality ?? placeMark.name {
                    address.append(subLocality)
                    address.append(", ")
                }
                if let city = placeMark.locality ?? placeMark.subAdministrativeArea {
                    address.append(city)
                    address.append(", ")
                }
                if let state = placeMark.administrativeArea, let country = placeMark.country {
                    address.append(state)
                    address.append(", ")
                    address.append(country)
                }

                // Adding Marker Details
                marker.title = placeName
                marker.snippet = address
                marker.appearAnimation = .pop
                marker.map = mapView
            }
        }
    }
}



extension HomeVC{
    private func renameAction (audioTitle:String){
        
        let fileName = audioTitle
        
        let alertController = UIAlertController(title: "Rename", message: "", preferredStyle: UIAlertController.Style.alert)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter name"
            textField.text = fileName
            textField.autocapitalizationType = .words
            textField.addTarget(self, action: #selector(self.alertTextFieldDidChange(_:)), for: .editingChanged)

        }
        
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let textFiled = alertController.textFields![0] as UITextField
            let newName = textFiled.text
            self.lblRecordName.text = newName
            
            
            if self.currentSelectedIndex != -1{
                self.audioDataSource[self.currentSelectedIndex].audioName = newName
                
                CoreDataManager.shared.updateCoreDataObject(object: self.audioDataSource[self.currentSelectedIndex])
            }

        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                                            (action : UIAlertAction!) -> Void in })
        
        
        
        saveAction.isEnabled = false
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func alertTextFieldDidChange(_ textField: UITextField) {

        let alert = self.presentedViewController as! UIAlertController
        
        let saveAction: UIAlertAction = alert.actions.last!
       // saveAction.isEnabled = textField.text!.count > 0 && textField.text!.count < 41
        saveAction.isEnabled = textField.text!.count > 0

        if let text = textField.text, text.count >= 41 {
            textField.text = String(text.dropLast(text.count - 41))
            return
        }
        
        if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            saveAction.isEnabled = false
        }
    }
}


extension HomeVC:PlayerManagerDelegate{
    func playerProgress(progress: CGFloat) {
        let curTime = PlayerManager.sharedInstance.playerCurrentTime()
        self.lblCurrentTime.text = "\(Int(curTime))"
        self.lblRemainingTime.text = "-\(Int(self.videoDuration)-Int(curTime))"
        self.volumeSlider.value = Float(progress)
    }
    
    func playerBufferStart() {
        
    }
    
    func playerBufferStop() {
        
    }
    
    func playerDidEnd() {
        
    }
    
    
}
