//
//  CoreDataManagert.swift
//  SecondPhoneTwinbit
//
//  Created by Saad on 3/9/20.
//  Copyright © 2020 TwinBit. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    let context = AppDelegate.viewContext
    
    func saveContext(entityName: String , errorText:String) {
        do{
            
            try self.context.save()
            
            print("Saved   \(entityName)")
            
            
        }catch{
            print(errorText)
            
        }
    }
    
    func addMediaToCoreData(creationDate:Date?,fileName:String?,fileURL:String?)->AudioEntity?{
        let newItem = NSEntityDescription.insertNewObject(forEntityName: "AudioEntity", into: self.context) as! AudioEntity
        newItem.audioName = fileName
        newItem.audioURL = fileURL
        newItem.creationDate = creationDate
        self.saveContext(entityName: "Video", errorText: "Error")
        
        return newItem
    }
    
    func updateCoreDataObject(object:AudioEntity){
        do{
            try object.managedObjectContext?.save()
        }catch{
            
        }
    }
    
    func isMediaExistInCoreData(mediaName:String){
        
    }
    
    func getMediaListFromCoreData() -> [AudioEntity] {
        var mediaList = [AudioEntity]()
        let request : NSFetchRequest <AudioEntity>  = AudioEntity.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        request.sortDescriptors = [sortDescriptor]
        
        do{
            mediaList = try self.context.fetch(request)
        }catch{
            
        }
        
        return mediaList
    }
    
    
    
}
