//
//  ViewController.swift
//  FirebaseAptitude
//
//  Created by Ashikur Rahman on 12/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: Constants.isLoggedIn){
            self.gotoLoginVC()
        }else{
            self.gotoSignupVC()
        }
    }


}

extension ViewController{
    private func gotoLoginVC(){
        let vc = UIStoryboard.init(name: Storyboard.Login, bundle: nil).instantiateViewController(identifier: "LoginVC") as! LoginVC
        appDelegate.window?.rootViewController = RootNC(rootViewController: vc)
        UIView.transition(with: appDelegate.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
        }, completion: nil)
        
    }
    
    private func gotoSignupVC(){
        let vc = UIStoryboard.init(name: Storyboard.Signup, bundle: nil).instantiateViewController(identifier: "SignupVC") as! SignupVC
        appDelegate.window?.rootViewController = RootNC(rootViewController: vc)
        UIView.transition(with: appDelegate.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
        }, completion: nil)
        
    }
}

