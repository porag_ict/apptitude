//
//  LoginVC.swift
//  FirebaseAptitude
//
//  Created by Ashikur Rahman on 12/11/21.
//

import UIKit
import FirebaseAuth

class LoginVC: UIViewController {
    
    private var iconClick = true
    
    @IBOutlet weak var emailTxtField:UITextField!{
        didSet{
            self.emailTxtField.delegate = self
        }
    }
    @IBOutlet weak var passwordTxtField:UITextField!{
        didSet{
            self.passwordTxtField.delegate = self
        }
    }
    
    @IBOutlet weak var btnLogin:UIButton!{
        didSet{
            self.btnLogin.addTarget(self, action: #selector(btnLoginAction), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var lblNotRegistered:UILabel!{
        didSet{
            self.setAttributedText()
        }
    }
    
    
    @IBOutlet weak var btnShowORHidePass:UIButton!{
        didSet{
            self.btnShowORHidePass.addTarget(self, action: #selector(btnShowORHidePassAction), for: .touchUpInside)
        }
    }
    
    
    @IBOutlet weak var signupView:UIView!{
        didSet{
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(tapGestureRecognizer:)))
            self.signupView.addGestureRecognizer(tap)
            self.signupView.isUserInteractionEnabled = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginVC{
    @objc private func btnLoginAction(){
        self.firebaseLogin()
    }
    
    private func gotoHome(){
        guard let vc = self.getViewContoller(storyboardName: Storyboard.Home, viewControllerName: "HomeVC")as? HomeVC else { return }
        
        self.pushTheView(vc: vc)
    }
    
    
    private func firebaseLogin(){
        
        if !Reach().isInternetAvailable(){
            self.noInternetAlert()
            return
        }
        
        guard let email = self.emailTxtField.text,let password = self.passwordTxtField.text else {
            self.showAlertWithMessage(title: "Alert", message: "Invalid email or Wrong password", actionTitle: "OK")
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if let error = error as NSError? {
            
            switch AuthErrorCode(rawValue: error.code) {
            case .operationNotAllowed:
                self.showAlertWithMessage(title: "Alert", message: "Operation not allowed", actionTitle: "OK")
                break
            case .userDisabled:
                self.showAlertWithMessage(title: "Alert", message: "User disabled", actionTitle: "OK")
                break
            case .wrongPassword:
                self.showAlertWithMessage(title: "Alert", message: "Wrong password", actionTitle: "OK")
                break
            case .invalidEmail:
                self.showAlertWithMessage(title: "Alert", message: "Invalid email", actionTitle: "OK")
                break
            default:
                print("Error: \(error.localizedDescription)")
            }
          } else {
            print("User signs in successfully")
            let userInfo = Auth.auth().currentUser
            let email = userInfo?.email
            
            self.gotoHome()
          }
        }
    }
}



extension LoginVC{
    
    @objc func handleTap(tapGestureRecognizer: UITapGestureRecognizer){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc private func btnSignupAction(){
        
    }
    @objc private func btnShowORHidePassAction(){
        if(iconClick == true) {
            self.passwordTxtField.isSecureTextEntry = false
        } else {
            self.passwordTxtField.isSecureTextEntry = true
        }
        
        self.iconClick = !self.iconClick
    }
    
}


extension LoginVC{
    private func setAttributedText(){
        
        
        let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        

        let partOne = NSMutableAttributedString(string: "Don’t have an account, yet? - ", attributes: yourAttributes)

        let combination = NSMutableAttributedString()

        combination.append(partOne)
        
        
        
        let underlineAttribute = [NSAttributedString.Key.foregroundColor: UIColor.red,NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue] as [NSAttributedString.Key : Any]
        let underlineAttributedString = NSAttributedString(string: "Create an account", attributes: underlineAttribute)
        combination.append(underlineAttributedString)
        self.lblNotRegistered.attributedText = combination
    }
}

extension LoginVC:UITextFieldDelegate{
    
}
