import UIKit
import Photos

class DocManager {
    
    
    static let shared = DocManager()
    
    var fileManager = FileManager.default
    
    private init(){
        
    }
    
    private func rootDirectory()->URL?{
        return self.fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    func getAudioURL(mediaName: String) -> URL?{
        return rootDirectory()?.appendingPathComponent(mediaName)
    }
    
    
    
    static func getDocumentDirectory() -> URL {
        
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    func deleteVideoFromFiles(path:URL){
       
        do{
            try FileManager.default.removeItem(at: path)
        }
        catch{
            print("Error Occured hhh ",Error.self)
        }
    }
    
}
