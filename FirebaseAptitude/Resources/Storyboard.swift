//
//  Storyboard.swift
//  FirebaseAptitude
//
//  Created by Ashikur Rahman on 12/11/21.
//

import Foundation

enum Storyboard {
    static let Login = "Login"
    static let Signup = "Signup"
    static let Main = "Main"
    static let Home = "Home"
}
