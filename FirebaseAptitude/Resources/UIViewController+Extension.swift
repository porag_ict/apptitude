//
//  UIViewController+Extension.swift
//  Onion Recorder
//
//  Created by Ashikur Rahman on 18/10/21.
//

import Foundation
import UIKit

extension UIViewController{
    
    //MARK:- Show general alerts
    
    public func showAlertWithMessage(title:String?,message:String?,actionTitle:String?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: actionTitle, style: .default, handler: nil)
        
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    public func noInternetAlertGeneral() {
        let alert = UIAlertController(title: "No Internet Connection", message: "Internet connection is required. Check device settings and try again.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .destructive , handler:{ (UIAlertAction) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    public func noInternetAlert() {
        let alert = UIAlertController(title: "No Internet Connection", message: "Internet connection is needed to upgrade to VIP. Check device settings and try again.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .destructive , handler:{ (UIAlertAction) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
