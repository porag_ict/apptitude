//
//  AutoLayoutHelper.swift
//  Fasting
//
//  Created by Twinbit Test on 30/9/20.
//  Copyright © 2020 twinbit. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    func loader()->UIAlertController  {
        let alert = UIAlertController(title: nil, message: "Please Wait", preferredStyle: .alert)
        
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
        indicator.style = .large
        alert.view.addSubview(indicator)
        present(alert, animated: true, completion: nil)
        return alert
    }
    
    func stopLoader(loader:UIAlertController) {
        DispatchQueue.main.async {
            loader.dismiss(animated: true, completion: nil)
        }
    }
    
    public func setUIInterfaceOrientationToLandScape(_ value: UIInterfaceOrientation) {
//        UIDevice.current.setValue(value.rawValue, forKey: "orientation")
    }
    public func setUIInterfaceOrientationToPortrait(_ value: UIInterfaceOrientation) {
//        UIDevice.current.setValue(value.rawValue, forKey: "orientation")
    }
    
    
    public func getViewContoller(storyboardName : String,viewControllerName : String)->UIViewController{
        
        if #available(iOS   13.0, *){
            
            return  UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(identifier: viewControllerName)
            
        }else{
            
            return  UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier : viewControllerName)
        }
        
        
    }
    
    func hideKeyboardWhenTappedAround() {
            let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
            tap.cancelsTouchesInView = false
            view.addGestureRecognizer(tap)
        }
        @objc func dismissKeyboard() {
            view.endEditing(true)
        }
    
    public func isEmailValid(_ email: String) -> Bool {
        
        let emailRegEx =  "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"
        
        var returnValue = true
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx, options: .caseInsensitive)
            
            let nsString = email as NSString
            
            let results = regex.matches(in: email, range: NSRange(location: 0, length: nsString.length))
            
            print("Email validation  result : \(results) ")
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return returnValue
    }
    
    var isModal: Bool {
        
        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
        
        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
    
    public func getAttributedText(string : String,font : String, fontcolor : UIColor,fontsize : CGFloat)->NSAttributedString{
        
        return NSAttributedString(string: "\(string)", attributes: [
            NSAttributedString.Key.font : UIFont(name: font, size: fontsize)!,
            NSAttributedString.Key.foregroundColor : fontcolor
        ])
        
    }
    
    
    
    public func autoTopBottomCalculateLessThan16(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let height = UIScreen.main.bounds.size.height
        
        print("Device Height = \(height)")
        print("Value : \(value)")
        print("Calculative value : \((value / idle) * height)")
        
        let idle = height > idle ? CGFloat(1024) : CGFloat(896)
        
        print("Idle : \(idle)")
        
        
        
        if height == idle {
            
            return value
            
        }else{
            
//            if ((value / idle) * height) <= 16 {
//
//
//                return CGFloat(16)
//            }
            return (value / idle) * height
            
        }
    }
    
    
    
    
    public func autoTopBottomCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let height = UIScreen.main.bounds.size.height
        
        print("Device Height = \(height)")
        print("Value : \(value)")
        print("Calculative value : \((value / idle) * height)")
        
        let idle = height > idle ? CGFloat(1024) : CGFloat(896)
        
        print("Idle : \(idle)")
        
        
        
        if height == idle {
            
            return value
            
        }else if value == CGFloat(16){
            
            return value
            
        } else{
            
            if ((value / idle) * height) <= 16 {
                
                
                return CGFloat(16)
            }
            return (value / idle) * height
            
        }
    }
    
    public func autoLeadingCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let width = UIScreen.main.bounds.size.width
        
        let idle = width >= CGFloat(768) ? CGFloat(768) : CGFloat(414)
        
        print("height : \(width)")
        
        if width == idle {
            
            return value
            
        }else if value == CGFloat(16){
            
            return value
            
        } else{
            if ((value / idle) * width) <= 16 {
                
                return CGFloat(16)
            }
            return (value / idle) * width
        }
    }
    
    public func autoHeightCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let height = UIScreen.main.bounds.size.height
        
        print("Device Height = \(height)")
        print("Value : \(value)")
        print("Calculative value : \((value / idle) * height)")
        
        let idle = height > idle ? CGFloat(1024) : CGFloat(896)
        
        if height == idle {
            
            return value
            
        }else if value == CGFloat(45){
            
            return value
            
        } else{
            
            if ((value / idle) * height) <= 45 {
                
                
                return CGFloat(45)
            }
            return (value / idle) * height
            
        }
        
    }
    
    public func autoWidthCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let width = UIScreen.main.bounds.size.width
        
        print("height : \(width)")
        
        let idle = width >= CGFloat(768) ? CGFloat(768) : CGFloat(414)
        
        if width == idle {
            
            return value
            
        }else{
            return (value / idle) * width
        }
    }
    
    public func pushTheView(vc : UIViewController){
        
        if let nav = self.navigationController {
            
            nav.pushViewController(vc, animated: true)
            
        }else{
            
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }
        
    }
    
}

//class DELEGATE {
//    
//    static func isPremiumUser()->Bool{
//        
//        return true
//    }
//}

extension UIColor {
    
//    @nonobjc class var white: UIColor {
//        return UIColor(white: 1.0, alpha: 1.0)
//    }
    
    @nonobjc class var lightRoyalBlue: UIColor {
        return UIColor(red: 77.0 / 255.0, green: 54.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var greyBlue: UIColor {
        return UIColor(red: 96.0 / 255.0, green: 118.0 / 255.0, blue: 131.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var whiteTwo: UIColor {
        return UIColor(white: 210.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var brightSkyBlue: UIColor {
        return UIColor(red: 0.0, green: 211.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var paleGrey: UIColor {
        return UIColor(red: 246.0 / 255.0, green: 247.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var red: UIColor {
        return UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
    }
    
    @nonobjc class var black: UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }
    
    @nonobjc class var veryLightBlue: UIColor {
        return UIColor(red: 231.0 / 255.0, green: 236.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var duskyBlue: UIColor {
        return UIColor(red: 66.0 / 255.0, green: 81.0 / 255.0, blue: 134.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var duskyBlueTwo: UIColor {
        return UIColor(red: 66.0 / 255.0, green: 82.0 / 255.0, blue: 134.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var duskyBlueThree: UIColor {
        return UIColor(red: 66.0 / 255.0, green: 80.0 / 255.0, blue: 134.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var dark: UIColor {
        return UIColor(red: 40.0 / 255.0, green: 44.0 / 255.0, blue: 64.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var powderBlue: UIColor {
        return UIColor(red: 183.0 / 255.0, green: 198.0 / 255.0, blue: 249.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleGreyTwo: UIColor {
        return UIColor(red: 245.0 / 255.0, green: 247.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var darkGreyBlue: UIColor {
        return UIColor(red: 55.0 / 255.0, green: 57.0 / 255.0, blue: 88.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var blueyGrey: UIColor {
        return UIColor(red: 152.0 / 255.0, green: 159.0 / 255.0, blue: 187.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightPeriwinkle: UIColor {
        return UIColor(red: 213.0 / 255.0, green: 216.0 / 255.0, blue: 221.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var blackTwo: UIColor {
        return UIColor(white: 55.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var blueGrey: UIColor {
        return UIColor(red: 137.0 / 255.0, green: 138.0 / 255.0, blue: 141.0 / 255.0, alpha: 1.0)
    }
    
}

extension UIView {

    public func autoTopBottomCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let height = UIScreen.main.bounds.size.height
        
        print("Device Height = \(height)")
        print("Value : \(value)")
        print("Calculative value : \((value / idle) * height)")
        
//        let idle = height > idle ? CGFloat(1024) : CGFloat(896)
        let idle = CGFloat(896)
        print("Idle : \(idle)")

        if height == idle {
            
            return value
            
        }else if value == CGFloat(16){
            
            return value
            
        } else{
            
            if ((value / idle) * height) <= 16 {
                
                
                return CGFloat(16)
            }
            return (value / idle) * height
            
        }
    }
    
    public func autoLeadingCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let width = UIScreen.main.bounds.size.width
        
        let idle = width >= CGFloat(768) ? CGFloat(768) : CGFloat(414)
        
        print("height : \(width)")
        
        if width == idle {
            
            return value
            
        }else if value == CGFloat(16){
            
            return value
            
        } else{
            if ((value / idle) * width) <= 16 {
                
                return CGFloat(16)
            }
            return (value / idle) * width
        } 
    }
    
    public func autoHeightCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let height = UIScreen.main.bounds.size.height
        
        print("Device Height = \(height)")
        print("Value : \(value)")
        print("Calculative value : \((value / idle) * height)")
        
        let idle = height > idle ? CGFloat(1024) : CGFloat(896)
        
        if height == idle {
            
            return value
            
        }else if value == CGFloat(45){
            
            return value
            
        } else{
            
            if ((value / idle) * height) <= 45 {
                
                
                return CGFloat(45)
            }
            return (value / idle) * height
            
        }
        
    }
    
    public func autoWidthCalculate(value : CGFloat,idle : CGFloat)->CGFloat{
        
        let width = UIScreen.main.bounds.size.width
        
        print("height : \(width)")
        
        let idle = width >= CGFloat(768) ? CGFloat(768) : CGFloat(414)
        
        if width == idle {
            
            return value
            
        }else{
            return (value / idle) * width
        }
    }
    
}

extension UIButton{
    func setHeptic() {
//        UIImpactFeedbackGenerator(style: .light).impactOccurred()
    }
}
