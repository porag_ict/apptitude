//
//  RootNC.swift
//  Ringtone
//
//  Created by Twnibit_Raihan on 26/10/21.
//  Copyright © 2021 Twinbit. All rights reserved.
//

import UIKit

class RootNC: UINavigationController {
    
    var rootNavVCDelegate: RootNavVCDelegate?
    var statusBarStyle: UIStatusBarStyle = .default
    var statusBarIsHidden = false
    override var prefersStatusBarHidden: Bool {
        return statusBarIsHidden
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationBar.isHidden = true
        ProtocolHelper.shared.rootNavVcDelegate = self
    }

}

extension RootNC: RootNavVCDelegate {
    func appearStatusBar(isHidden: Bool) {
        self.statusBarIsHidden = isHidden
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func setStatusBarStyle(statusBarStyle: UIStatusBarStyle) {
        self.statusBarStyle = statusBarStyle
        setNeedsStatusBarAppearanceUpdate()
    }
}
