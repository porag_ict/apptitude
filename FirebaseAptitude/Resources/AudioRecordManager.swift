//
//  AKAudioRecorder.swift
//  AKAudioRecorder
//
//  Created by Aaryan Kothari on 22/04/20.
//  Copyright © 2020 Aaryan Kothari. All rights reserved.
//

import Foundation
import AVFoundation

class AudioRecordManager: NSObject {
    
    //MARK:- Instance
    static let shared = AudioRecordManager()
    
    var fileName : String?
    var isRecording : Bool = false
    
    
    //MARK:- Variables ( Private )
    private var audioSession : AVAudioSession = AVAudioSession.sharedInstance()
    private var audioRecorder : AVAudioRecorder!
    private var audioPlayer : AVAudioPlayer = AVAudioPlayer()
    
    private var settings =   [  AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                                AVSampleRateKey: 12000,
                                AVNumberOfChannelsKey: 1,
                                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue ]

    
    

    //MARK:- Pre - Recording Setup
    private func InitialSetup(){
        fileName = NSUUID().uuidString   ///  unique string value
        let audioFilename = getDocumentsDirectory().appendingPathComponent(fileName!.appending(".m4a"))

        do{
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.isMeteringEnabled = true
            audioRecorder.prepareToRecord()
        } catch let audioError as NSError {
            print ("Error setting up: %@", audioError)
        }
    }
    
    
    //MARK:- Record
    func startRecord(){
        InitialSetup()
        if let audioRecorder = audioRecorder{
            if !isRecording {
                do{
                    try audioSession.setActive(true)
                    isRecording = true
                    
                    print("Record running")
                    audioRecorder.record() /// start recording
                } catch let recordingError as NSError{
                    print ("Error recording : %@", recordingError.localizedDescription)
                }
            }
        }
    }
    
    
    //MARK:- Stop Recording
    func stopRecording(){
        if audioRecorder != nil{
            audioRecorder.stop() /// stop recording
            audioRecorder = nil
            do {
                try audioSession.setActive(false)
                isRecording = false
            } catch {
                print("stop()",error.localizedDescription)
            }
            
        }
    }
    
    func getAudioFileUrl() -> URL? {
        
        if let name = self.fileName {
            let fileName = name + ".m4a"
            let path = getDocumentsDirectory().appendingPathComponent(fileName)
            return path
        }
        else {
            return nil
        }
    }
    
    
    func playByUrl(url: URL) {
        
        if FileManager.default.fileExists(atPath: url.path){
            audioPlayer.prepareToPlay()
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer.delegate = self
                audioPlayer.play()  /// play
            } catch {
                print("play(with name:), ",error.localizedDescription)
            }
        } else {
            print("File Does not Exist")
            return
        }
    }


    //MARK:- Delete Recording
    func deleteRecording(url: URL){
        //let path = getDocumentsDirectory().appendingPathComponent(name.appending(".m4a"))
        let manager = FileManager.default
        
        if manager.fileExists(atPath: url.path) {
            
            do {
                try manager.removeItem(at: url)
            } catch {
                print("delete()",error.localizedDescription)
            }
        } else {
            print("File is not exist.")
        }
    }

    
    
    //MARK:- Get path
    private func getDocumentsDirectory() -> URL {
         let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
         return paths[0]
     }
    
    
    func getRecordedSongList()->[URL]{
        
        var mp3Files = [URL]()
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        do {
            
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: [.creationDateKey]).sorted(by: {
                
                let date0 = try $0.promisedItemResourceValues(forKeys:[.contentModificationDateKey]).contentModificationDate!
                
                let date1 = try $1.promisedItemResourceValues(forKeys:[.contentModificationDateKey]).contentModificationDate!
                
                return date0.compare(date1) == .orderedDescending
            })
            
            
            mp3Files = directoryContents.filter{ $0.pathExtension == "m4a"}
            
        } catch {
            
        }
        
        return mp3Files
    }
    
    
    
}


//MARK:- AVAudioRecorder Delegate functions
extension AudioRecordManager : AVAudioRecorderDelegate{
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        isRecording = false
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        isRecording = false
    }
}


//MARK:- AVAudioPlayer Delegate functions
extension AudioRecordManager: AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
    }
}

