//
//  PlayerManager.swift
//  MusicPlayer_cred
//
//  Created by Siddhant Mishra on 30/10/19.
//  Copyright © 2019 Siddhant Mishra. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation



protocol PlayerManagerDelegate {
    
    func playerProgress(progress: CGFloat)
    
    func playerBufferStart()
    func playerBufferStop()
    func playerDidEnd()
    
    
}



class PlayerManager{
    
    var audioPlayer: AVPlayer?
    private var playTimer = Timer()
    var avPlayerObserver:Any?
    
    var isSpinnerActive:Bool = false
    
    
    var delegate:PlayerManagerDelegate?
    
    
    static let sharedInstance = PlayerManager()
    
    var songURL:URL?
    
    private init() {}
    
    
    
    //MARK:- Player
    
    func initialisePlayer(withUrl url : NSURL)  {
        
        if (self.audioPlayer != nil) {
            self.audioPlayer?.pause()
            self.audioPlayer = nil
        }
        let playerItem = AVPlayerItem(url: url as URL)
        
        self.audioPlayer = AVPlayer(playerItem:playerItem)
        
        audioPlayer?.automaticallyWaitsToMinimizeStalling = false
        
        
        avPlayerObserver = self.audioPlayer?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 1), queue: DispatchQueue.main, using: { time in
            
        })
        
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(itemDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil)
        
    }
    
    
    
    func playerCurrentTime()->Int{
        var curTime = 0
        if self.audioPlayer !=  nil{
            curTime = Int(self.audioPlayer?.currentTime().seconds ?? 0)
        }
        
        return curTime
    }
    
    func playAudio(withUrl url : NSURL)  {
        
        self.isSpinnerActive = true
        self.delegate?.playerBufferStart()
        
        if (self.audioPlayer != nil) {
            self.audioPlayer?.pause()
            self.audioPlayer = nil
        }
        let playerItem = AVPlayerItem(url: url as URL)
        
        self.songURL = url as URL
        self.audioPlayer = AVPlayer(playerItem:playerItem)
        
        self.playerPlay()
        audioPlayer?.automaticallyWaitsToMinimizeStalling = false
        
        
        avPlayerObserver = self.audioPlayer?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 1), queue: DispatchQueue.main, using: { time in
            
            if self.audioPlayer?.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                
                if (self.audioPlayer?.currentItem?.isPlaybackLikelyToKeepUp) != nil {
                    self.isSpinnerActive = false
                    self.delegate?.playerBufferStop()
                }
            }
        })
        
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(itemDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil)
        
    }
    
    func playerPlay(){
        
        
        
        audioPlayer?.play()
        
        playTimer.invalidate()
        
        playTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updatingSeekingSliderValue), userInfo: nil, repeats: true)
        
        //  RunLoop.current.add(playTimer, forMode: RunLoop.Mode.default)
    }
    
    func playerPause(){
        
        playTimer.invalidate()
        
        if audioPlayer != nil{
            audioPlayer?.pause()
        }
    }
    
    func playerIsPlaying() -> Bool{
        
        if audioPlayer?.rate != 0 && audioPlayer?.error == nil {
            return true
        }else{
            return false
        }
    }
    
    @objc func updatingSeekingSliderValue(){
        
        let totalValue:CGFloat =  CGFloat(CMTimeGetSeconds(audioPlayer?.currentItem?.duration ?? CMTime(value: 0, timescale: 1)))
        let currentValue:CGFloat =  CGFloat(CMTimeGetSeconds(audioPlayer?.currentTime() ?? CMTime(value: 0, timescale: 1)))
        
        let progress: CGFloat =  currentValue/totalValue
        
        self.delegate?.playerProgress(progress: progress)
    }
    
    @objc func itemDidFinishPlaying(){
        
        self.playerPause()
        audioPlayer?.seek(to: CMTime(value: Int64(0), timescale: 1))
        self.delegate?.playerProgress(progress: 0)
        self.delegate?.playerDidEnd()
        
        // self.playerPlay()
    }
    
    func changePlayerVolume(volume:Float){
        self.audioPlayer?.volume = volume
    }
    func seekAudio(to pos:CGFloat){
        let time: CMTime = CMTimeMakeWithSeconds(Float64(pos), preferredTimescale: self.audioPlayer?.currentTime().timescale ?? 6000)
        self.audioPlayer?.seek(to: time, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
    }
    
    func mutePlayer(){
        self.audioPlayer?.isMuted = !(self.audioPlayer?.isMuted ?? true)
    }
}


extension PlayerManager: CachingPlayerItemDelegate {
    
    func playerItem(_ playerItem: CachingPlayerItem, didFinishDownloadingData data: Data) {
        print("File is downloaded and ready for storing")
        
        
        
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, didDownloadBytesSoFar bytesDownloaded: Int, outOf bytesExpected: Int) {
        print("\(bytesDownloaded)/\(bytesExpected)")
    }
    
    func playerItemPlaybackStalled(_ playerItem: CachingPlayerItem) {
        print("Not enough data for playback. Probably because of the poor network. Wait a bit and try to play later.")
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, downloadingFailedWith error: Error) {
        print(error)
    }
    
}
