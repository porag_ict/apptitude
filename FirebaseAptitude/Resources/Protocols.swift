//
//  Protocols.swift
//  Ringtone
//
//  Created by Twnibit_Raihan on 8/11/21.
//  Copyright © 2021 Twinbit. All rights reserved.
//

import Foundation
import UIKit

class ProtocolHelper {
    static let shared = ProtocolHelper()
    var customTabRootDelegate: CustomTabRootDelegate?
    var rootNavVcDelegate: RootNavVCDelegate?
    
}

protocol CustomTabRootDelegate {
    func hideTabBar()
    func showTabBar()
    func appearStatusBar(isHidden: Bool)
}

protocol RootNavVCDelegate {
    func setStatusBarStyle(statusBarStyle: UIStatusBarStyle)
    func appearStatusBar(isHidden: Bool)
}
