//
//  TimeFormatter.swift
//  Ringtone
//
//  Created by Twnibit_Raihan on 10/11/21.
//  Copyright © 2021 Twinbit. All rights reserved.
//

import Foundation

struct TimeFormat {

    var totalSeconds: Int

    var years: Int {
        return totalSeconds / 31536000
    }

    var days: Int {
        return (totalSeconds % 31536000) / 86400
    }

    var hours: Int {
        return (totalSeconds % 86400) / 3600
    }

    var minutes: Int {
        return (totalSeconds % 3600) / 60
    }

    var seconds: Int {
        return totalSeconds % 60
    }

    //simplified to what OP wanted
    var hoursMinutesAndSeconds: (hours: Int, minutes: Int, seconds: Int) {
        return (hours, minutes, seconds)
    }
}


extension TimeFormat {

    var simpleTimeString: String {
        let hoursText = timeText(from: hours)
        let minutesText = timeText(from: minutes)
        let secondsText = timeText(from: seconds)
        return "\(hoursText):\(minutesText):\(secondsText)"
    }

    private func timeText(from number: Int) -> String {
        return number < 10 ? "0\(number)" : "\(number)"
    }
}


class TimeFormatter{
    static func timeInSeconds(time:Int)->String{
        let watch = TimeFormat(totalSeconds: time)
        return watch.simpleTimeString
    }
}
