//
//  AudioEntity+CoreDataProperties.swift
//  FirebaseAptitude
//
//  Created by Ashikur Rahman on 17/11/21.
//
//

import Foundation
import CoreData


extension AudioEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AudioEntity> {
        return NSFetchRequest<AudioEntity>(entityName: "AudioEntity")
    }

    @NSManaged public var audioName: String?
    @NSManaged public var audioURL: String?
    @NSManaged public var creationDate: Date?

}

extension AudioEntity : Identifiable {

}
